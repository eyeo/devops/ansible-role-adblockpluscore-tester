# Manage Adblock Plus test resources via Ansible

This is an [Ansible Role][role] for setting up an enviroment, where the
Adblockplus Core module can be tested in an automated fashion.

[role]: https://docs.ansible.com/ansible/latest/user_guide/playbooks_reuse_roles.html
